/* App frontend script */
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    if (n > slides.length) {
        slideIndex = 1 
        $(".number_index").text(slideIndex);
    }else{
        $(".number_index").text(n);
    }
    if (n < 1) { 
        slideIndex = slides.length;
        $(".number_index").text(slideIndex);
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        
    }
    if (slides.length > 0) {
        slides[slideIndex - 1].style.display = "block";
    }
}
$(document).ready(function () {
    $("#saveProjectId").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/save-config',
            type: 'POST',
            data: { projectId: $("input#projectIdTestworthy").val(), appId: AP._hostOrigin.replace("https://", "") },
            success: function (responseText) {
                $(".message_change").fadeIn();
                setTimeout(function(){
                    $(".message_change").fadeOut();
                },1500)
            },
            error: function (xhr, statusText, errorThrown) {
                console.log(arguments);
            }
        });

    });
})