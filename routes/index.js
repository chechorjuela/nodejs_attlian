const axios = require('axios');
const sqlite = require('sqlite3').verbose();
const config = require('../credentials.json');

var db = new sqlite.Database('./db/testworthy.db');
db.run('CREATE TABLE IF NOT EXISTS projects(id integer primary key, projectId text, appId text)');

const host = 'https://empletestworthxay.atlassian.net';
const token = config.hosts['https://exampletestworthy.atlassian.net'].password;
const username = config.hosts['https://exampletestworthy.atlassian.net'].username;

export default function routes(app, addon) {
  app.get('/', (req, res) => {
    res.redirect('/atlassian-connect.json');
  });

  app.get('/config-page', addon.authenticate(), (req, res) => {
    var projectID = '';
    db.each(`SELECT id,projectId FROM projects WHERE appId like '${req.context.hostBaseUrl.replace("https://", "")}'`, [], function (error, row) {
      if (error) {
        res.render(
          'configure-page.hbs',
          {
            title: 'Atlassian Connect',
            value: projectID
          }
        );
      } else {
        projectID = row.projectId;
        res.render(
          'configure-page.hbs',
          {
            title: 'Atlassian Connect',
            value: projectID
          }
        );
      }
    });
  });
  app.post('/save-config', function (req, res) {
    let data = { status: 200, message: '' };
    db.each(`SELECT id,projectId FROM projects WHERE appId like '${req.body.appId}'`, [], function (error, row) {
      if (error) {
        data.status = 403;
        data.message = error.message;
        res.send(data);
      }
      if (typeof row != "undefined") {
        if (row.projectId != req.body.projectId) {
          db.run(`UPDATE projects SET projectId = '${req.body.projectId}' WHERE appId='${req.body.appId}'`, [], error => {
            if (error) {
              data.status = 403;
              data.message = error.message
            } else {
              data.message = "updated";
              res.send(data);
            }
          })
        }
      } else {
        db.run(`INSERT INTO(projectId,appId) projects  VALUES('${req.body.projectId}','${req.body.appId}')`, [], error => {
          if (error) {
            data.status = 403;
            data.message = error.message;
            res.send(data);
          } else {
            data.message = "insert";
            res.send(data);
          }
        })
      }
    })
    res.send(data);
  });

  app.get('/testworthy', addon.authenticate(), (req, res) => {

    db.each(`SELECT id,projectId FROM projects WHERE appId like '${req.context.hostBaseUrl.replace("https://", "")}'`, [], function (error, row) {
      if (error) {

      } else {
        axios.get(`http://10pdev.us/api/testresult/defectinfo?jiraTicket=TW-522&projectId=${row.projectId}`).then(resp => {
          res.render(
            'testworthy.hbs',
            {
              title: 'Atlassian Connect',
              cases: resp.data,
              total: resp.data.length
            });
        })
      }
    });

  });
}
